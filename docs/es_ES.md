# Header picture menu

===================================

 

### Description

  
Este menú tiene la opción de añadir una imagen en la cabecera. Debajo de dicha imagen aparecerá el texto del menu con la posibilidad deañadir información extra.

  
  

### Configuration

Para configurar este menú debes elegir una imageny pegagr la dirección en el area designada para ello. Debes seleccionar las diferentes opciones para el texto del menú. Debajo de cada opción hay un área en la que puedes escribir información extra.