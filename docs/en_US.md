# Header picture menu

===================================

 

### Description

  

This menu has the option of adding a background image. below this image you'll be able to add a text menu with some extra information per line.
  
  

### Configuration

To configure this module choose an image and paste the url inside the designated area. You must do the same with all the different options for the text of the menu. As an extra option there is a text area where you'll be able to give some extra information about what inside every option.